import "./App.css";
import { useState, useEffect } from "react";
import Graph from "./Graph/Graph";
import {
  CITIES,
  days,
  months_abbr,
  months,
  WEATHER_FORECAST,
} from "./Constants";

// eslint-disable-next-line no-extend-native
Date.prototype.toShortFormat = function (friendlyFormat = false) {
  const day = this.getDate();
  const dayName = days[this.getDay() + 1];
  const monthNameAbbr = months_abbr[this.getMonth()];
  const monthName = months[this.getMonth()];
  const year = this.getFullYear();
  const hour = this.getHours();
  const min = this.getMinutes();

  if (friendlyFormat)
    return `${hour}:${min} - ${dayName} ${day} ${monthName}, ${year}`;
  return `${day} ${monthNameAbbr} ${year}`;
};

function App() {
  const [selectedCity, setSelectedCity] = useState(CITIES[0]);
  const [activeTab, setActiveTab] = useState(0);
  const [hourlyTemp, setHourlyTemp] = useState();
  const [hourlyPrec, setHourlyPrec] = useState();
  const [hourlyWind, setHourlyWind] = useState();
  const [temp, setTemp] = useState();
  const [weekly, setWeekly] = useState(null);
  const [minTemp, setMinTemp] = useState();
  const [maxTemp, setMaxTemp] = useState();
  const [minPrec, setMinPrec] = useState();
  const [maxPrec, setMaxPrec] = useState();
  const [minWind, setMinWind] = useState();
  const [maxWind, setMaxWind] = useState();
  const [icon, setIcon] = useState();

  useEffect(() => {
    const cityID = selectedCity.id;
    const weatherForecast = WEATHER_FORECAST.find((item) => item.id === cityID);
    async function fetchData() {
      setHourlyTemp(weatherForecast.hourlyTemp);
      setTemp(weatherForecast.temp);
      setWeekly(weatherForecast.weekly);
      setMinTemp(weatherForecast.minTemp);
      setMaxTemp(weatherForecast.maxTemp);
      setMinPrec(weatherForecast.minPrec);
      setMaxPrec(weatherForecast.maxPrec);
      setMinWind(weatherForecast.minWind);
      setMaxWind(weatherForecast.maxWind);
      setIcon(weatherForecast.icon);
      setHourlyPrec(weatherForecast.hourlyPrecipitation);
      setHourlyWind(weatherForecast.hourlyWindSpeed);
    }
    fetchData();
  }, [selectedCity]);
  const today = new Date();

  if (
    !hourlyTemp ||
    !hourlyPrec ||
    !hourlyWind ||
    !weekly ||
    !temp ||
    !maxTemp ||
    !minTemp
  )
    return <p>Loading...</p>;
  return (
    <div className="forecast-app-container d-flex">
      <div className="left-column">
        <div className="left-header d-flex">
          <span>POINTR</span>
          <div className="dropdown">
            <span className="d-flex">
              Select a city <span className="material-icons">menu</span>
            </span>
            <div className="dropdown-content">
              {CITIES.map((city) => {
                return (
                  <div
                    key={city.id}
                    className="menu-item"
                    onClick={() => setSelectedCity(city)}
                  >
                    {city.name}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="forecast-content">
          <div className="temp-info d-flex">
            <span className="material-icons ">{icon}</span>
            <p>{temp}</p>
          </div>
          <p>{selectedCity.name}</p>
          <p>{selectedCity.country}</p>
          <p>{today.toShortFormat(true)}</p>
        </div>
      </div>
      <div></div>
      <div className="right-column d-flex">
        <div className="header-container">
          <h3>Regional Weather Forecast</h3>
          <p>{today.toShortFormat()}</p>
          <div className="tab-container d-flex">
            <span
              className={activeTab === 0 ? "active" : ""}
              onClick={() => setActiveTab(0)}
            >
              Temperature
            </span>
            <span
              className={activeTab === 1 ? "active" : ""}
              onClick={() => setActiveTab(1)}
            >
              Precipitation
            </span>
            <span
              className={activeTab === 2 ? "active" : ""}
              onClick={() => setActiveTab(2)}
            >
              Wind
            </span>
          </div>
        </div>
        <div className="tab-underlines d-flex">
          <div className={activeTab === 0 ? "active" : ""} />
          <div className={activeTab === 1 ? "active" : ""} />
          <div className={activeTab === 2 ? "active" : ""} />
        </div>
        <div className="graph-section d-flex">
          {activeTab === 0 && (
            <div className="graph-box d-flex">
              <div className="header d-flex">
                <h4>Hourly Temperature Change (°C)</h4>
                <div>
                  <span className="material-icons">wb_sunny</span>
                  {`${minTemp}/${maxTemp}`}
                </div>
              </div>
              <div className="graph-container">
                <Graph dataArray={hourlyTemp} label="Temperature" />
              </div>
            </div>
          )}

          {activeTab === 1 && (
            <div className="graph-box d-flex">
              <div className="header d-flex">
                <h4>Hourly Precipitation Data (mm)</h4>
                <div>
                  <span className="material-icons">water_drop</span>
                  {`${minPrec}/${maxPrec}`}
                </div>
              </div>
              <div className="graph-container">
                <Graph dataArray={hourlyPrec} label="Precipitation" />
              </div>
            </div>
          )}

          {activeTab === 2 && (
            <div className="graph-box d-flex">
              <div className="header d-flex">
                <h4>Hourly Wind Speed (mph)</h4>
                <div>
                  <span className="material-icons">air</span>
                  {`${minWind}/${maxWind}`}
                </div>
              </div>
              <div className="graph-container">
                <Graph dataArray={hourlyWind} label="Wİnd" />
              </div>
            </div>
          )}
        </div>
        <div className="weekly-sum d-flex">
          {weekly.map((week) => (
            <div className="day-info d-flex" key={week.day}>
              <div>{week.day}</div>
              <span className="material-icons">{week.icon}</span>
              <div>{`${week.min}-${week.max}`}</div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
