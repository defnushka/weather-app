import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    x: {
      grid: {
        color: "#FFFFFF",
        drawBorder: false,
      },
      ticks: {
        color: "#ffffff",
      },
    },
    y: {
      grid: {
        display: false,
        color: "#FFFFFF",
      },
      ticks: {
        color: "#ffffff",
      },
    },
  },
  plugins: {
    legend: {
      display: false,
      labels: {
        fontColor: "white",
      },
    },
    title: {
      display: false,
    },
  },
};
function Graph({ dataArray, label }) {
  const data = {
    labels: dataArray.map((singleSlot) => singleSlot.x),
    datasets: [
      {
        label: label,
        data: dataArray.map((singleData) => singleData.y),
        borderColor: "rgb(255, 255, 255)",
      },
    ],
  };
  return <Line options={options} data={data} />;
}

export default Graph;
